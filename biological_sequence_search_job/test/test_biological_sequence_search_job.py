"""
This Module tests the basic functions of the job
"""
import unittest

import xml.etree.ElementTree as ET

from biological_sequence_search_job import results_loader


class TestBiologicalSearches(unittest.TestCase):
    """
    Class to test the functions used in the biological searches
    """

    def test_loads_results_from_xml(self):
        """
        Tests 1
        """
        sample_data_file_path = 'biological_sequence_search_job/test/data/sample_results.xml'

        with open(sample_data_file_path, 'rt') as sample_data_file:
            xml_text = sample_data_file.read()
            results_root = ET.fromstring(xml_text)

            ebi_schema_url = results_loader.get_ebi_schema_url()
            results_path = f'{ebi_schema_url}SequenceSimilaritySearchResult/{ebi_schema_url}hits'
            raw_blast_results = results_root.find(results_path)

            results = results_loader.get_results_list(raw_blast_results)

            num_results_must_be = len(raw_blast_results)
            num_results_got = len(results)

            self.assertEqual(num_results_must_be, num_results_got, msg='The results where not loaded correctly!')

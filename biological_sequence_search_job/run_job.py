#!/usr/bin/env python3
"""
Script that runs a job that does sequence searches
"""
import argparse
from pathlib import Path
import json
import time

import yaml

from biological_sequence_search_job.server_connection import DelayedJobsServerConnection
from biological_sequence_search_job import blast_search

PARSER = argparse.ArgumentParser()
PARSER.add_argument('run_params_file', help='The path of the file with the run params')
PARSER.add_argument('-v', '--verbose', help='Make output verbose', action='store_true')
PARSER.add_argument('-n', '--dry-run', help='Do not actually send progress to the delayed jobs server',
                    action='store_true')
ARGS = PARSER.parse_args()


def run():
    """
    Runs the job
    """
    print('RUN BLAST!')

    initial_time = time.time()

    run_params = yaml.load(open(ARGS.run_params_file, 'r'), Loader=yaml.FullLoader)
    job_params = run_params.get('job_params')

    search_params = job_params
    search_params = add_sticky_chembl_params(search_params)

    server_conn = DelayedJobsServerConnection(run_params_file_path=ARGS.run_params_file, dry_run=ARGS.dry_run)

    search_results, time_taken_in_ebi_service = blast_search.get_blast_results(search_params, server_conn)

    output_path = run_params.get('output_dir')
    output_file_path = Path.joinpath(Path(output_path), 'results.json')
    print('going to write results to: ', str(output_file_path))
    with open(output_file_path, 'wt') as output_file:
        output_file.write(json.dumps(search_results))

    server_conn.update_job_progress(100, f'Output file written!!!.')

    print('time_taken_in_ebi_service: ')
    print(time_taken_in_ebi_service)

    end_time = time.time()
    time_taken = end_time - initial_time
    stats_dict = {
        'time_taken': time_taken
    }
    server_conn.send_job_statistics(stats_dict)


def add_sticky_chembl_params(search_params):
    """
    Adds to the dict passed as parameter some parameters fixed that will always be added to each query
    :param search_params: search params dict
    """
    return {
        **search_params,
        'stype': 'protein',
        'program': 'blastp',
        'database': 'chembl',
        'email': 'chembl@ebi.ac.uk'
    }


if __name__ == "__main__":
    run()

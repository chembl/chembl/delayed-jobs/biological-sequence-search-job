"""
Module with functions for loading the results of the search
"""
import re
import xml.etree.ElementTree as ET

import requests
from dateutil import parser

BLAST_API_BASE_URL = 'https://www.ebi.ac.uk/Tools/services/rest/ncbiblast'


def get_search_results(job_id):
    """
    :param job_id: id of the job in the BLAST service
    :return: a dictionary with the search results loaded from the server and the time it took in the EBI service
    """
    raw_blast_results, time_taken = get_results_xml_root(job_id)
    results = get_results_list(raw_blast_results)

    search_results = {
        'search_results': results
    }
    return search_results, time_taken


def get_xml_url(job_id):
    """
    :param job_id: id of the job
    :return: The url to get the results in xml format
    """
    return f'{BLAST_API_BASE_URL}/result/{job_id}/xml'


def get_results_xml_root(job_id):
    """
    :param job_id: id of the job
    :return: the root of the xml tree with the results and the time it took
    """

    xml_url = get_xml_url(job_id)
    print('xml_url: ', xml_url)

    xml_request = requests.get(xml_url)
    xml_response = xml_request.text
    results_root = ET.fromstring(xml_response)

    ebi_schema_url = get_ebi_schema_url()
    results_path = f'{ebi_schema_url}SequenceSimilaritySearchResult/{ebi_schema_url}hits'
    blast_results = results_root.find(results_path)

    time_path = f'{ebi_schema_url}Header/{ebi_schema_url}timeInfo'
    start_time = parser.parse(results_root.find(time_path).get('start'))
    end_time = parser.parse(results_root.find(time_path).get('end'))
    time_taken = (end_time - start_time).total_seconds()

    return blast_results, time_taken


def get_ebi_schema_url():
    """
    :return: the ebi schema url
    """
    return '{http://www.ebi.ac.uk/schema}'


def get_results_list(raw_blast_results):
    """
    :param raw_blast_results: raw results xml root
    :return: the processed results list
    """

    ebi_schema_url = get_ebi_schema_url()
    results = []
    id_regex = re.compile(r'CHEMBL\d+')
    best_alignment_path = f'{ebi_schema_url}alignments/{ebi_schema_url}alignment'
    score_path = f'{ebi_schema_url}score'
    score_bits_path = f'{ebi_schema_url}bits'
    identities_path = f'{ebi_schema_url}identity'
    positives_path = f'{ebi_schema_url}positives'
    expectation_path = f'{ebi_schema_url}expectation'

    for result_child in raw_blast_results:
        result_id = id_regex.match(result_child.get('id')).group()
        length = result_child.get('length')
        best_alignment = result_child.find(best_alignment_path)
        best_score = float(best_alignment.find(score_path).text)
        best_score_bits = float(best_alignment.find(score_bits_path).text)
        best_identities = float(best_alignment.find(identities_path).text)
        best_positives = float(best_alignment.find(positives_path).text)
        best_expectation = float(best_alignment.find(expectation_path).text)

        new_result = {
            'target_chembl_id': result_id,
            'length': length,
            'best_score': best_score,
            'best_score_bits': best_score_bits,
            'best_identities': best_identities,
            'best_positives': best_positives,
            'best_expectation': best_expectation
        }
        results.append(new_result)

    return results

"""
Module that handles the communication with the EBI system to do the BLAST search
"""
import json
from urllib.parse import urlencode
from urllib.request import urlopen, Request
from urllib.error import HTTPError
import time

import requests

from biological_sequence_search_job import results_loader

BLAST_API_BASE_URL = 'https://www.ebi.ac.uk/Tools/services/rest/ncbiblast'
BLAST_WEB_BASE_URL = 'https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi'


class BLASTSearchError(Exception):
    """base class for errors in this module"""


def get_blast_results(search_params, server_connection):
    """
    :param search_params: search params for BLAST
    :param server_connection: connection with the server
    :return: a dict with the blast results obtained
    """
    server_connection.update_job_progress(0, 'Going to submit search to the EBI Service')
    job_id = submit_blast_job(search_params)
    print('job_id: ', job_id)
    status_dict = get_status_dict(job_id)
    server_connection.update_job_progress(10, f'Job submitted. Job id: {job_id}', json.dumps(status_dict))

    try:

        server_connection.update_job_progress(10, f'Waiting until job finishes', json.dumps(status_dict))
        wait_until_job_finishes(job_id)
        server_connection.update_job_progress(50, f'Job finished in the EBI service. Now going to load the results.',
                                              json.dumps(status_dict))

        search_results, time_taken = results_loader.get_search_results(job_id)

        server_connection.update_job_progress(90, f'Results Loaded!!!.', json.dumps(status_dict))

        return search_results, time_taken

    except BLASTSearchError as error:

        status_dict = get_status_dict(job_id, str(error))
        server_connection.update_job_progress(10, f'Search Job Failed: {str(error)}', json.dumps(status_dict))


def get_status_dict(job_id, error_msg=None):
    """
    :param job_id: id of the job
    :param error_msg: error message if any
    :return: a dictionary with scribing the status of the job in the EBI system
    """
    status_dict = {
        'ebi_job_id': job_id,
        'status_url': get_job_status_url(job_id),
        'web_url': get_web_job_page_url(job_id),
        'xml_url': results_loader.get_xml_url(job_id)
    }

    if error_msg is not None:
        status_dict['error_msg'] = error_msg

    return status_dict


def submit_blast_job(search_params):
    """
    Submits the job to the EBI service
    :param search_params: parameters for BLAST
    :return: the id of the job
    """

    print('GET BLAST RESULTS')
    run_url = get_base_submission_url()
    print('run_url: ', run_url)
    print('search_params:')
    print(json.dumps(search_params, indent=4))

    request_data = urlencode(search_params)
    print('request_data: ', request_data)

    try:

        req = Request(run_url)
        req_handle = urlopen(req, request_data.encode(encoding=u'utf_8', errors=u'strict'))
        job_id = req_handle.read().decode(encoding=u'utf_8', errors=u'strict')
        req_handle.close()
        return job_id

    except HTTPError as ex:

        msg = f'Error while submitting BLAST job:\n{repr(ex)}'
        raise BLASTSearchError(msg)


def wait_until_job_finishes(job_id):
    """
    checks the status of the job and returns until it finishes
    """
    status = None
    print('Waiting until job finishes')

    while status != 'FINISHED':
        status = get_sequence_search_status(job_id)

        if status == 'NOT_FOUND':
            raise BLASTSearchError('Search submission not found, it may have expired. Please run the search again.')
        if status == 'FAILURE':
            raise BLASTSearchError('There was an error in the EBI BLAST Search.')
        if status == 'QUEUED':
            time.sleep(1)
            continue
        if status == 'RUNNING':
            time.sleep(1)
            continue
        if status == 'FINISHED':
            return
        raise BLASTSearchError(f'Status {status} not recognised')


def get_sequence_search_status(job_id):
    """
    Gets the status of the BLAST job given its id
    :param job_id: id of the job to check
    :return: status of the job as returned by the EBI Service
    """

    status_url = f'{BLAST_API_BASE_URL}/status/{job_id}'
    print('status_url: ', status_url)
    status_request = requests.get(status_url)
    status_response = status_request.text

    return status_response


def get_base_submission_url():
    """
    :return: the base url to submit a BLAST Job to the EBI service
    """
    return f'{BLAST_API_BASE_URL}/run/'


def get_job_status_url(job_id):
    """
    :param job_id: id of the BLAST job
    :return: the url to get the status of the BLAST job in the EBI system
    """
    return f'{BLAST_API_BASE_URL}/status/{job_id}'


def get_web_job_page_url(job_id):
    """
    :param job_id: id of the BLAST job
    :return: the web version of the BLAST search job
    """
    return f'{BLAST_WEB_BASE_URL}?jobId={job_id}'

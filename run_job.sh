#!/usr/bin/env bash

set -x
set -e

source /app/python_env/bin/activate
PYTHONPATH=/app/python_env/lib/python3.7/site-packages/:/app python3 /app/biological_sequence_search_job/run_job.py $1
deactivate
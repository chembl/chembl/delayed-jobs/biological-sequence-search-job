#!/usr/bin/env bash

set -x

# ----------------------------------------------------------------------------------------------------------------------
# Simulate simple BLAST job
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_1 || true
mkdir -p $PWD/functional_tests/output_1
PYTHONPATH=$PWD:$PYTHONPATH $PWD/biological_sequence_search_job/run_job.py $PWD/functional_tests/params/run_params_example1.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_1/results.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate BLAST job with some extra parameters
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_2 || true
mkdir -p $PWD/functional_tests/output_2
PYTHONPATH=$PWD:$PYTHONPATH $PWD/biological_sequence_search_job/run_job.py $PWD/functional_tests/params/run_params_example2.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_2/results.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate BLAST job with more parameters
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_3 || true
mkdir -p $PWD/functional_tests/output_3
PYTHONPATH=$PWD:$PYTHONPATH $PWD/biological_sequence_search_job/run_job.py $PWD/functional_tests/params/run_params_example3.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_3/results.json
